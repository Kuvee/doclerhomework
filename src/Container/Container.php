<?php
/**
 * Created by PhpStorm.
 * User: Kuvee
 * Date: 2017.02.28.
 * Time: 21:16
 */

namespace Language\Container;

use fool\echolog\Echolog;
use Language\Api\ApiCaller;
use Language\File\LanguageFileHandler;
use Language\File\AppletLanguageFileHandler;
use Pimple\Container as PimpleContainer;
use Psr\Log\LogLevel;

abstract class Container
{
    /**
     * @var \Pimple\Container
     */
    private static $container;

    /**
     * @return \Pimple\Container
     */
    public static function getInstance() {
        if (self::$container === null) {
            self::$container = new PimpleContainer();
        }
        return self::$container;
    }

    public static function init() {
        $container = self::getInstance();

        $container["LOG_TRESHOLD"] = LogLevel::INFO;

        $container["logger"] = function ($c) {
            return new Echolog($c["LOG_TRESHOLD"]);
        };

        $container["apiCaller"] = function ($c) {
            $apiCaller = new ApiCaller();
            $apiCaller->setLogger($c["logger"]);
            return $apiCaller;
        };

        $container["LanguageFileHandler"] = $container->factory(function ($c) {
            $lfh = new LanguageFileHandler();
            $lfh->setLogger($c["logger"]);
            $lfh->setApiCaller($c["apiCaller"]);
            return $lfh;
        });

        //AppletLanguageFileHandler
        $container["AppletLanguageFileHandler"] = $container->factory(function ($c) {
            $alfh = new AppletLanguageFileHandler();
            $alfh->setLogger($c["logger"]);
            $alfh->setApiCaller($c["apiCaller"]);
            return $alfh;
        });
    }
}