<?php
/**
 * Created by PhpStorm.
 * User: Kuvee
 * Date: 2017.03.05.
 * Time: 18:48
 */

namespace Language\File;

class LanguageFileHandler extends AbstractFileHandler
{
    private $application;
    private $language;

    protected function setLanguageData($data)
    {
        if (empty($data['application']) || empty($data['language'])) {
            throw new FileHandlerException("Invalid language data");
        }
        $this->application = $data['application'];
        $this->language = $data['language'];
    }

    protected function getFileContent()
    {
        return $this->apiCaller->getLanguageFile($this->language);
    }

    protected function getFilePath()
    {
        return $this->getBaseDirectory($this->application) . $this->language . '.php';
    }

}