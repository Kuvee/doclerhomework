<?php
/**
 * Created by PhpStorm.
 * User: Kuvee
 * Date: 2017.03.05.
 * Time: 18:29
 */

namespace Language\File;


use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Language\Api\ApiCaller;
use Language\Config;

abstract class AbstractFileHandler implements LoggerAwareInterface
{
    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ApiCaller
     */
    protected $apiCaller;

    /**
     * @param LoggerInterface $logger
     * @return null
     * @codeCoverageIgnore
     */
    public final function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param ApiCaller $apiCaller
     * @codeCoverageIgnore
     */
    public function setApiCaller(ApiCaller $apiCaller)
    {
        $this->apiCaller = $apiCaller;
    }

    /**
     * @param mixed $data
     * @return bool
     */
    public function handleLanguageFile($data) {
        $this->setLanguageData($data);
        $path = $this->getFilePath();
        $this->createDirectoryIfNotExists($path);
        $content = $this->getFileContent();
        return $this->writeFileContent($path, $content);
    }

    /**
     * @param mixed $data
     * @return mixed
     */
    abstract protected function setLanguageData($data);

    /**
     * @return string
     */
    abstract protected function getFileContent();

    /**
     * @return string
     */
    abstract protected function getFilePath();

    /**
     * @param string $path
     */
    protected function createDirectoryIfNotExists($path)
    {
        if (!is_dir(dirname($path))) {
            mkdir(dirname($path), 0755, true);
        }
    }

    /**
     * @param string $finalDir
     * @return string
     */
    protected function getBaseDirectory($finalDir)
    {
        return Config::get('system.paths.root') . '/cache/' . $finalDir . '/';
    }

    /**
     * @param string $path
     * @param string $content
     * @return bool
     */
    protected function writeFileContent($path, $content)
    {
        return (bool)file_put_contents($path, $content);
    }


}