<?php
/**
 * Created by PhpStorm.
 * User: Kuvee
 * Date: 2017.03.05.
 * Time: 18:48
 */

namespace Language\File;


use Language\Config;

class AppletLanguageFileHandler extends AbstractFileHandler
{
    private $applet;
    private $language;

    protected function setLanguageData($data)
    {
        if (empty($data['applet']) || empty($data['language'])) {
            throw new FileHandlerException("Invalid language data");
        }
        $this->applet = $data['applet'];
        $this->language = $data['language'];
    }

    protected function getFileContent()
    {
        return $this->apiCaller->getAppletLanguageFile($this->applet, $this->language);
    }

    protected function getFilePath()
    {
        return $this->getBaseDirectory("flash") . 'lang_' . $this->language . '.xml';
    }

}