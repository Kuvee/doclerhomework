<?php

namespace Language;
use Language\Api\ApiCaller;
use Language\Api\ApiException;
use Language\Container\Container;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;

/**
 * Business logic related to generating language files.
 */
class LanguageBatchBo implements LoggerAwareInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ApiCaller
     */
    private $apiCaller;

    private $applets = array(
        'memberapplet' => 'JSM2_MemberApplet',
    );

    public function __construct()
    {
        Container::init();
        $this->setLogger(Container::getInstance()["logger"]);
        $this->setApiCaller(Container::getInstance()["apiCaller"]);
    }

    /**
     * @param LoggerInterface $logger
     * @codeCoverageIgnore
     * @return null
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param ApiCaller $apiCaller
     * @codeCoverageIgnore
     */
    public function setApiCaller(ApiCaller $apiCaller)
    {
        $this->apiCaller = $apiCaller;
    }

    /**
     * Starts the language file generation.
     */
	public function generateLanguageFiles()
	{
	    $applications = Config::get('system.translated_applications');

        $this->logger->debug("Generating language files");
		foreach ($applications as $application => $languages) {
            $this->logger->debug("APPLICATION: \" . $application");
			foreach ($languages as $language) {
                $this->logger->debug("LANGUAGE: \" . $language");

                try {
                    Container::getInstance()["LanguageFileHandler"]->handleLanguageFile(["language" => $language, "application" => $application]);
                    $this->logger->info("Language file successfully created:" . $language);
                } catch (ApiException $e) {
                    $this->logger->error('API error during getting language file: (' . $application . '/' . $language . '): ' . $e->getMessage());
                } catch (\Exception $e) {
                    $this->logger->error('Unable to generate language file:' . $language . ', ' . $e->getMessage());
                }
			}
		}
	}


    /**
     * Gets the language files for the applet and puts them into the cache.
     */
	public function generateAppletLanguageXmlFiles()
	{
        $this->logger->debug("Getting applet language XMLs..");
		foreach ($this->applets as $appletDirectory => $appletLanguageId) {
            $this->logger->debug(" Getting > $appletLanguageId ($appletDirectory) language xmls..");

            try {
                $languages = $this->apiCaller->getAppletLanguages($appletLanguageId);
            } catch (ApiException $e) {
                $this->logger->error('Getting languages for applet (' . $appletLanguageId . ') was unsuccessful: ' . $e->getMessage());
                continue;
            }

            $this->logger->debug(" - Available languages: " . implode(', ', $languages));
			foreach ($languages as $language) {
                $this->logger->debug("LANGUAGE: \" . $language");

                try {
                    Container::getInstance()["AppletLanguageFileHandler"]->handleLanguageFile(["language" => $language, "applet" => $appletLanguageId]);
                    $this->logger->info("Language xml successfully cached: $appletLanguageId ($appletDirectory / $language)");
                } catch (ApiException $e) {
                    $this->logger->error('API error during getting language file: (' . $appletLanguageId . '/' . $language . '): ' . $e->getMessage());
                } catch (\Exception $e) {
                    $this->logger->error('Unable to generate language file:' . $language . ', ' . $e->getMessage());
                }
            }
		}
	}
}
