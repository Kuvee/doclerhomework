<?php

namespace Language\Test\Api;

use Language\Api\ApiCaller;
use Language\Container\Container;

class ApiCallerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ApiCaller
     */
    private $apiCaller;

    CONST APPLET_NAME = "JSM2_MemberApplet";

    public function setUp()
    {
        $this->apiCaller = Container::getInstance()["apiCaller"];
    }

    public function testGetAppletLanguagesHasEnglish()
    {
        $languages = $this->apiCaller->getAppletLanguages(self::APPLET_NAME);
        $this->assertSame(["en"], $languages);
    }

    public function testGetAppletLanguageFileNotEmpty()
    {
        $appletLanguageFileContent = $this->apiCaller->getAppletLanguageFile(self::APPLET_NAME, "en");
        $this->assertNotEmpty($appletLanguageFileContent);
    }

    public function testGetLanguageFileNotEmpty() {
        $languageFileContent = $this->apiCaller->getLanguageFile("en");
        $this->assertNotEmpty($languageFileContent);
    }

}