<?php

namespace Language\Test\Api;

use Language\File\LanguageFileHandler;
use Language\Container\Container;
use Language\File\FileHandlerException;

class LanguageFileHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var LanguageFileHandler
     */
    private $lfh;

    public function setUp()
    {
        $this->lfh = Container::getInstance()["AppletLanguageFileHandler"];
    }

    /**
     * @param $language
     * @param $application
     *
     * @dataProvider providerTestHandleLanguageFile
     */
    public function testHandleLanguageFile($language, $application)
    {
        $this->lfh->handleLanguageFile(["language" => $language, "applet" => $application]);
    }

    /**
     * @param $language
     * @param $application
     *
     * @dataProvider providerTestHandleLanguageFileInvalidArguments
     */
    public function testHandleLanguageFileInvalidArguments($language, $application) {
        try {
            $this->lfh->handleLanguageFile(["language" => $language, "applet" => $application]);
        } catch (FileHandlerException $e) {}
    }

    public function providerTestHandleLanguageFile()
    {
        return [
            ["language" => "hu", "application" => "app1"],
            ["language" => "en", "application" => "app1"],
            ["language" => "de", "application" => "app2"],
            ["language" => "hu", "application" => "app5"],
        ];
    }

    public function providerTestHandleLanguageFileInvalidArguments()
    {
        return [
            ["language" => "hu", "application" => null],
            ["language" => null, "application" => "app1"],
            ["language" => null, "application" => null],
        ];
    }



}