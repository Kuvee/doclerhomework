<?php

namespace Language\Test\Api;

use Language\File\AppletLanguageFileHandler;
use Language\Container\Container;
use Language\File\FileHandlerException;

class AppletLanguageFileHandlerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var AppletLanguageFileHandler
     */
    private $alfh;

    public function setUp()
    {
        $this->alfh = Container::getInstance()["AppletLanguageFileHandler"];
    }

    /**
     * @param $language
     * @param $applet
     *
     * @dataProvider providerTestHandleLanguageFile
     */
    public function testHandleLanguageFile($language, $applet)
    {
        $this->alfh->handleLanguageFile(["language" => $language, "applet" => $applet]);
    }

    /**
     * @param $language
     * @param $applet
     *
     * @dataProvider providerTestHandleLanguageFileInvalidArguments
     */
    public function testHandleLanguageFileInvalidArguments($language, $applet) {
        try {
            $this->alfh->handleLanguageFile(["language" => $language, "applet" => $applet]);
        } catch (FileHandlerException $e) {}
    }

    public function providerTestHandleLanguageFile()
    {
        return [
            ["language" => "hu", "applet" => "app1"],
            ["language" => "en", "applet" => "app1"],
            ["language" => "de", "applet" => "app2"],
            ["language" => "hu", "applet" => "app5"],
        ];
    }

    public function providerTestHandleLanguageFileInvalidArguments()
    {
        return [
            ["language" => "hu", "applet" => null],
            ["language" => null, "applet" => "app1"],
            ["language" => null, "applet" => null],
        ];
    }



}