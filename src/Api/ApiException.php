<?php
/**
 * Created by PhpStorm.
 * User: Kuvee
 * Date: 2017.02.28.
 * Time: 23:02
 */

namespace Language\Api;


use Exception;

class ApiException extends \Exception
{

    private $type;
    private $data;

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    public function __construct($messageBase, $type = null, $code = null, $data = null)
    {
        $this->type = $type;
        $this->data = $data;
        $message = $messageBase
            . (!empty($type) ? " Type($type)" : '')
            . (!empty($code) ? " Code($code)" : '')
            . (!empty($data) ? " Data(".json_encode($data).")" : '');
        parent::__construct($message, $code);
    }

}