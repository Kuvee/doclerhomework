<?php
/**
 * Created by PhpStorm.
 * User: Kuvee
 * Date: 2017.02.28.
 * Time: 22:31
 */

namespace Language\Api;


use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerInterface;
use Language\ApiCall;

class ApiCaller implements LoggerAwareInterface
{
    const API_DEFAULT_TARGET = "system_api";
    const API_DEFAULT_MODE = "language_api";
    const API_DEFAULT_SYSTEM = "LanguageFiles";

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @param LoggerInterface $logger
     * @return null
     * @codeCoverageIgnore
     */
    public function setLogger(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param string $language
     * @return string
     * @throws ApiException
     */
    public function getLanguageFile($language)
    {
        return $this->call('getLanguageFile', ["language" => $language]);
    }

    /**
     * @param string $applet
     * @return string[]
     * @throws ApiException
     */
    public function getAppletLanguages($applet)
    {
        return $this->call('getAppletLanguages', ["applet" => $applet]);
    }

    /**
     * @param string $applet
     * @param string $language
     * @return string
     * @throws ApiException
     */
    public function getAppletLanguageFile($applet, $language)
    {
        return $this->call('getAppletLanguageFile', ['applet' => $applet, 'language' => $language]);
    }

    /**
     * @param string $action
     * @param array $postAttributes
     * @param string $target
     * @param string $mode
     * @param string $system
     * @return mixed
     * @throws ApiException
     */
    private function call(
        $action,
        $postAttributes = [],
        $target = self::API_DEFAULT_TARGET,
        $mode = self::API_DEFAULT_MODE,
        $system = self::API_DEFAULT_SYSTEM
    ) {
        $this->logger->debug("Initiating Api call: {$target}/{$mode}/{$system}/{$action}/".json_encode($postAttributes));

        $result = $this->checkApiResult(ApiCall::call(
            $target,
            $mode,
            [
                "system" => $system,
                "action" => $action
            ],
            $postAttributes
        ))['data'];

        $this->logger->debug("Successful Api call, data: ".json_encode($result));


        return $result;
    }

    /**
     * @param mixed $result
     * @return mixed
     * @throws ApiException
     */
    private function checkApiResult(&$result)
    {
        // Error during the api call.
        if ($result === false || !isset($result['status'])) {
            throw new ApiException('Error during the api call');
        }

        // Wrong response.
        if ($result['status'] != 'OK') {
            throw new ApiException(
                'Wrong response:',
                (!empty($result['error_type']) ? $result['error_type'] : null),
                (!empty($result['error_code']) ? $result['error_code'] : null),
                (!empty($result['data']) ? $result['data'] : null)
            );
        }

        // Wrong content.
        if (empty($result['data'])) {
            throw new ApiException('Wrong content!');
        }

        return $result;
    }

}